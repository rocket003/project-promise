﻿/*******************
 ** File: 	 SniffArrow.cs
 ** Project: Promise
 ** Author:  Beth
 ** Date:    10/14/16
 **
 **   		 This code destroys an arrow after Lifetime amount of seconds.
 **
 *********************************/

using UnityEngine;
using System.Collections;

public class SniffArrow : MonoBehaviour {

    public int Lifetime;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, Lifetime);
	}
}
