﻿using UnityEngine;
using System.Collections;

public class Boar : MonoBehaviour {
	public float BoarStartingPoint;
	public float BoarEndingPoint;
	private GameObject dog;
	private GameObject borkedUp;
	private Rigidbody2D boarMovement;
	private RaycastHit2D hit;

	void Start () {
		dog = GameObject.Find("Bork");
		borkedUp = GameObject.Find ("You Borked up!");
		boarMovement = GetComponent<Rigidbody2D>();
	}
	
	void Update () {
		// this if statement will run when the dog is past a certain point and 
		// the boar has not reached a certain point (cliff) 
		if (dog.transform.position.x > BoarStartingPoint && transform.position.x <= BoarEndingPoint){
			boarMovement.velocity = new Vector2(8f,0);
		}
	
	}
	
	void FixedUpdate(){
		// Raycast is used to dectect objects that the boar has to jump over
		hit = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x,5f);
		if(hit.collider != null && hit.collider.gameObject.tag.Equals("jumpOver")) {
			boarMovement.AddForce(new Vector2(1000f, 5000f));
		}
		
		// Raycast is used to detect if the boar hit the dog
		// dog will be deactivated if this happens
		if(hit.collider != null && hit.collider.gameObject.tag.Equals("Player") && dog.transform.position.x > BoarStartingPoint) {
			borkedUp.GetComponent<SpriteRenderer>().enabled = true;
		}
	}
}
