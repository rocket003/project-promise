﻿/*******************
 ** File: 	 Move.cs
 ** Project: Promise
 ** Author:  Beth
 ** Date:    9/30/16
 **
 **   		 This is a cleaned up, rewritten version of the Move script. 
 **			 -It contains the move, sprint, and jump actions. 
 **			 -It might also contain a crouch action in the future? 
 **			 -All animation updates are currently commented out. 
 **
 **			 For the jump and falling animations to work:
 **			 - Create an empty game object named groundCheck as a child of the sprite
 **			 - Place it right underneath the sprite
 **			 - Set your newly created groundCheck object as the Unity move script Ground Check variable 
 **			 - Set What Is Ground to all layers except Player. 
 **			 
 **
 **			 A lot of the code is from: https://unity3d.com/learn/tutorials/topics/2d-game-creation/2d-character-controllers
 **
 *********************************/

using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
	
	// Variables affecting movement magnitude 
	public float speed = 10f;
	public float sprintScale = 2f;
	public float jump = 700f;
	
	// Helper bools 
	private bool facingRight = true;
	private bool spacePressed = false;
	
	// Grounded variables
	private bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.2f;
	public LayerMask whatIsGround;

	// Objects for manipulating physics and animations
	private Rigidbody2D rigidbodyComponent;
	private Animator anim;
	
	
	void Start () {
		// Pull sprite info 
		rigidbodyComponent = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
	}
	
	
	// All input checks are in Update
	void Update() {
		// Jump if Space is pressed and sprite is currently on ground
		if(grounded && Input.GetKeyDown(KeyCode.Space)) {
			spacePressed = true;
		}
		
		// Sprint if LShift held down   
		if( Input.GetKeyDown( KeyCode.LeftShift ) ) {
			speed *= sprintScale;
		}
        if( Input.GetKeyUp( KeyCode.LeftShift ) ) {
			speed /= sprintScale;
		}
	}
	

	// All physic manipulations are in FixedUpdate
	void FixedUpdate () {
		// Falling 
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround); 		// Will return true if sprite is touching the ground
		//*******anim.SetBool("Ground", grounded);													// Plays falling animation if sprite is not touching ground 
		//******anim.SetFloat("vSpeed", rigidbodyComponent.velocity.y);								// Determines falling animation tree
		
		// Jump 
		if (spacePressed) {
			//*******anim.SetBool("Ground", false);
			rigidbodyComponent.AddForce(new Vector2(0, jump));				// Push Sprite jump magnitude up
			spacePressed = false;
		}
		
		// Move
		float move = Input.GetAxisRaw("Horizontal");                        // Get player input
        //*********anim.SetFloat("Speed", Mathf.Abs(move));
        anim.SetFloat("Speed", Mathf.Abs(move));
        rigidbodyComponent.velocity = new Vector2(move * speed, rigidbodyComponent.velocity.y);		// Apply movement to rigid body (i.e. move the sprite)
		
		// Chang Direction 
		if (move > 0 && !facingRight)
			Flip();
		else if (move < 0 && facingRight) 
			Flip();
	}
	
	
	// Flips sprite over the y-axis
	void Flip() {
		facingRight = !facingRight;
		Vector3 spriteScale = transform.localScale;			// Get sprite's current scale
		spriteScale.x *= -1;								// Negate the x scale (i.e. flip horizontally)
		transform.localScale = spriteScale;					// Apply new scale back to sprite
	}
}
