﻿/*******************
 ** File: 	 Sniff.cs
 ** Project: Promise
 ** Author:  Beth
 ** Date:    10/14/16
 **
 **   		 This code searches for sniffable objects in a large circle collider.
 **          If sniffable objects are in range and the player presses "F", it will
 **          create arrows above every sniffable object in range. 
 **
 **          In order for this code to work, you must:
 **             - Create an arrow prefab and attach the SniffArrow script to it (and set a Lifetime)
 **             - Create an emtpy object as a child of the dog. 
 **                 - Give the object a large circle collider that is set to IsTrigger. 
 **                 - Attach the Sniff script to the child object. 
 **                 - Drag your arrow prefab into the arrow public key. 
 **             - Set any sniffable objects' tags to "Sniffable"
 **
 *********************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sniff : MonoBehaviour {
	
    public Transform arrow;
	Animator anim;
    
	// Have list of objects that are currently in sniff range 
	List<GameObject> objectsInRange = new List<GameObject>();

	void Start(){

		anim = transform.parent.gameObject.GetComponent<Animator> ();
	}
	
	// Detect when sniffable object enters range
	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Sniffable") {
			// Add object that triggered collider to List 
			GameObject item = other.gameObject;
			objectsInRange.Add(item);
		}
	}
	
	// Detect when sniffable object exits range 
	void OnTriggerExit2D(Collider2D other) {
		if (other.tag == "Sniffable") {
			GameObject item = other.gameObject;
			objectsInRange.Remove(item);
		}
			
	}
	
	// Update is called once per frame
	void Update () {
		// Check if player sniffed (pressed 'F')
		if( Input.GetKeyDown( KeyCode.F ) ) {
			// Sniff animation should go here *****
			Debug.Log("Sniff sniff");
			anim.SetTrigger ("Sniff");   //Double tapping F before the animation finishes causes the trigger to get stuck and the animation infinitely loops somehow. FIX LATER
			
			// Point to each sniffable object in range 
			for (int i = 0; i < objectsInRange.Count; i++) {
                // Find the objects position
                Vector3 arrowPosition = objectsInRange[i].transform.position;
                // Add its height
                arrowPosition = arrowPosition + new Vector3(0, objectsInRange[i].GetComponent<Renderer>().bounds.size.y, 0);
                
                // Create a new arrow object 
                Transform sniffArrow = (Transform)Instantiate(arrow, arrowPosition, Quaternion.identity);
			}
		}
	}
}
