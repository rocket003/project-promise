﻿using UnityEngine;
using System.Collections;

/*
NOTES:
-For the jumping mechanism to work, add an empty object under the parent of this object and place it on the very bottom-middle point of the object. 
    Drag the new child object to "Ground Point" in this script component. After that, set "Ground Mask" to the same layer as the ground object. 
    The child object is suppose to act as a sensor to detect if the object is on the ground.

*/
public class Movement : MonoBehaviour
{

    public float movespeed;
    public int jumpHeight;
    public SpriteRenderer spriteRen;

    public Transform groundPoint;
    public float radius;
    public LayerMask groundMask;
    bool isGrounded;
    bool facing = true;
    float baseSpeed;

    RaycastHit2D groundDistCheck;
    public float groundCheckDist = 3f;
    public float groundCheckStart = 1.5f;
    float curYpos; 


    Rigidbody2D rb2D;
    Animator anim;


    // Use this for initialization
    void Start()
    {

        rb2D = GetComponent<Rigidbody2D>(); //gets the rigidbody off of the character
        spriteRen = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        baseSpeed = movespeed;
    }

    // Update is called once per frame
    void Update()
    {

        Vector2 moveDir = new Vector2(Input.GetAxisRaw("Horizontal") * movespeed, rb2D.velocity.y);
        rb2D.velocity = moveDir;
        anim.SetFloat("Speed", Mathf.Abs(Input.GetAxisRaw("Horizontal")));
        isGrounded = Physics2D.OverlapCircle(groundPoint.position, radius, groundMask);


        if (Input.GetAxisRaw("Horizontal") == -1 && facing == true)
        {

            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            facing = false;

        }
        else if (Input.GetAxisRaw("Horizontal") == 1 && facing == false)
        {

            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            facing = true;
        }

        //stays in jump frame while it is still in air
		if (!isGrounded)
			anim.SetBool ("inAir", true);
		else
			anim.SetBool ("inAir", false);

        //start jump
		if (Input.GetKeyDown(KeyCode.Space) && isGrounded && anim.GetBool("Jump") == false)
        {
            rb2D.AddForce(new Vector2(0, jumpHeight));
			anim.SetTrigger("Jump");
        }

        //while dog is in the air AND falling
        //draw debug.drawRayCast
        if (!isGrounded && transform.position.y - curYpos < 0)
        {
            Physics2D.queriesStartInColliders = false;
            //Physics2D.Raycast
            groundDistCheck = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - groundCheckStart), -Vector2.up, 1f);
			if (groundDistCheck.collider != null && groundDistCheck.collider.gameObject.layer == 8 && anim.GetBool("Landing") == false)
            {
                //play fall back to ground animation
				anim.SetBool("inAir", false);
				anim.SetTrigger ("Landing");

            }
        }

        //sprinting
        if (Input.GetKeyDown(KeyCode.LeftShift) && isGrounded)
        {
            movespeed = movespeed * 2.5f;
        }
        else if(Input.GetKeyUp(KeyCode.LeftShift) && isGrounded)
        {
            movespeed = baseSpeed;
        }

        curYpos = transform.position.y;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(groundPoint.position, radius);
		//Gizmos.DrawLine(new Vector2(transform.position.x, transform.position.y - groundCheckStart), -Vector2.up, 1f);
    }
}